//
//  ChooseInputViewController.swift
//  ImageEditorVK
//
//

import Foundation
import UIKit

class ChooseInputViewController: UIViewController {
    var blurView: UIVisualEffectView?
    
    @IBOutlet weak var buttonsFolderView: UIView!

    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!

    @IBOutlet weak var galleryView: UIView!
    @IBOutlet weak var galleryImage: UIImageView!
    @IBOutlet weak var galleryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeTopBar()
        updateViews()
    }
    
    private func updateViews() {
        let superViewWidth = view.frame.width
        let cameraViewHeight = (superViewWidth - 8 * 3) / 2
        
        buttonsFolderView.frame = CGRect(x: buttonsFolderView.frame.minX, y: buttonsFolderView.frame.minY, width: view.frame.width, height: cameraViewHeight)
        
        buttonsFolderView.center = view.center

        cameraView.frame = CGRect(x: 8, y: 8, width: cameraViewHeight, height: cameraViewHeight)
        galleryView.frame = CGRect(x: 8 * 2 + cameraViewHeight, y: 8, width: cameraViewHeight, height: cameraViewHeight)
        
        galleryButton.frame = galleryView.frame
        cameraButton.frame = cameraView.frame
        
        cameraImage.frame = CGRect(x: 0, y: 0, width: cameraViewHeight, height: cameraViewHeight)
        galleryImage.frame = CGRect(x: 0, y: 0, width: cameraViewHeight, height: cameraViewHeight)

        cameraView.backgroundColor = .clear
        galleryView.backgroundColor = .clear
    }
    
    private func changeTopBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    
    @IBAction func openCameraAction(_ sender: Any) {
        print("openCameraAction")
    }
    @IBAction func openGalleryAction(_ sender: Any) {
        print("openGalleryAction")
    }
}
