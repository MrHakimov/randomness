//
//  NewController.swift
//  ImageEditorVK
//
//

import Foundation
import UIKit

class NewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate,  UITabBarDelegate {
    @IBSegueAction func cameraSegue(_ coder: NSCoder) -> NewController? {
        resType = .camera
        return NewController(coder: coder)
    }
    
    @IBSegueAction func folderSegue(_ coder: NSCoder) -> NewController? {
        resType = .folder
        return NewController(coder: coder)
    }
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var ZoomScrollView: UIScrollView!
    //    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet weak var imageView: UIImageView!
    let pickerController = UIImagePickerController()
    
    @IBOutlet weak var scrollViewAscept: NSLayoutConstraint!
    
    enum ResType {
        case folder, camera
    }

    var resType: ResType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ZoomScrollView.backgroundColor = .black
        self.view.backgroundColor = .black
        
        self.ZoomScrollView.maximumZoomScale = 20
        self.ZoomScrollView.minimumZoomScale = 1
        imageView.frame = CGRect(x: 0, y: 0, width: imageView.frame.width, height: imageView.frame.height)
        pickerController.modalPresentationStyle = .popover
        pickerController.delegate = self
        tabBar.delegate = self
        
        if resType == .camera {
            openCamera()
        } else {
            openGallery()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cameraSegue" {
            resType = .camera
        } else {
            resType = .folder
        }
    }

    func imagePickerController(_ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFit
            imageView.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
        
        let imageSize: CGSize!
        if imageView.image == nil {
            imageSize = CGSize(width: 1, height: 1)
        } else {
            imageSize = imageView.image!.size
        }

//        let newConstraint = scrollViewAscept.constraintWithMultiplier((imageSize.width) / (imageSize.height))
//        ZoomScrollView.removeConstraint(scrollViewAscept)
//        view.addConstraint(newConstraint)
//        view.layoutIfNeeded()
//        scrollViewAscept = newConstraint
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView.self
    }
    
    private func changeTopBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            openCamera()
        case 1:
            openGallery()
        case 4:
            imageView.image = imageView.image?.imageRotatedByDegrees(flipUp: true, flipLeft: false)
        case 3:
            imageView.image = imageView.image?.imageRotatedByDegrees(flipUp: false, flipLeft: true)
        default:
            print(item)
        }
    }
    
    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerController.SourceType.camera
            //picker.cameraDevice = UIImagePickerController.CameraDevice.front
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    private func openGallery() {
        pickerController.allowsEditing = false
        pickerController.sourceType = .photoLibrary

        present(pickerController, animated: true, completion: nil)
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

extension UIImage {
    public func imageRotatedByDegrees(flipUp: Bool, flipLeft: Bool) -> UIImage {
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint.zero, size: size))
        let rotatedSize = rotatedViewBox.frame.size

        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()!

        bitmap.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0)
        var yFlip: CGFloat
        var xFlip: CGFloat

        if flipUp {
            yFlip = CGFloat(-1.0)
        } else {
            yFlip = CGFloat(1.0)
        }
        
        if flipLeft {
            xFlip = CGFloat(-1.0)
        } else {
            xFlip = CGFloat(1.0)
        }
        
        bitmap.scaleBy(x: yFlip, y: xFlip)
        bitmap.draw(self.cgImage!,
                    in: CGRect.init(x: -size.width / 2,
                                    y: -size.height / 2,
                                    width: size.width,
                                    height: size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return newImage
    }
}
